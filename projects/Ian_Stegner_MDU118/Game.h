#pragma once

#include "app.h"
#include "map.h"
#include "entity.h"
#include "player.h"

class Game : public App
{
public:
	Game();
	virtual ~Game();
	virtual bool start();
	virtual void update(float deltaT);
	virtual void render();
	virtual void cleanup();
	static Game &inst();
	bool playMode;

	sf::Texture *m_backgroundTexture;
	sf::Sprite *m_backgroundSprite;

	sf::Sprite * m_tiles;

	Map m_map;

	Tile m_currentTile;

	float moveDelay;

	std::vector<Entity *> m_entities;
	Player *m_player;
	sf::View defaultView;
	void Save();
	void Load();
	void Clear();
};
