#pragma once
#include "SFML\Graphics.hpp"

class Entity {
public:
	int x;
	int y;
	float moveDelay = 0.5;
	bool isBlocking;
	sf::Sprite *sprite;
	virtual void update(float deltaT);
	virtual void render(sf::RenderWindow *win);
};

class Enemy : public Entity {
public:
	Enemy();
};