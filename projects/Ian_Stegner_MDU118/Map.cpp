#pragma once
#include "map.h"
#include "qgf2d\sfml_util.h"

void Map::SetMapSize(int width, int height) 
{
	tiles.resize(width*height);
	mapWidth = width;
	mapHeight = height;
}

Map::Map()
{
	tileWidth = 32;
	tileHeight = 32;
}

void Map::Render(sf::RenderWindow* window)
{
	for (int y = 0; y < mapHeight; y++) 
	{
		for (int x = 0; x < mapWidth; x++) 
		{
			auto tile = tiles[x + y * mapWidth];
			qgf::selectSpriteTile1D(atlas, tile.tileIndex, tileWidth, tileHeight);
			atlas->setPosition(x * tileWidth, y * tileHeight);
			window->draw(*atlas);
		}
	}
}

void Map::SetTile(Tile tile, int x, int y) 
{
	if (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight) 
	{
		tiles[x + y * mapWidth] = tile;
	}
	else
	{
		return;
	}
}

int Map::FindPlayerInMap() 
{
	for (int y = 0; y < mapHeight; ++y)
	{
		for (int x = 0; x < mapWidth; ++x)
		{
			Tile t = GetTile(x, y);
			if (t.tileIndex == 4)
			{
				return x + y * mapWidth;
				break;
			}
			
		}
	}
	return 0;
}

Tile Map::GetTile(int x, int y) 
{
	if (x < mapWidth && y < mapHeight) 
	{
		return tiles[x + y * mapWidth];
	}
	return Tile();
}