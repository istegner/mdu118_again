#include "Game.h"
#include "player.h"
#include "windows.h"

Game::Game() : App()
{
}

Game::~Game()
{
}

Game &Game::inst()
{
	static Game s_instance;
	return s_instance;
}

bool Game::start()
{
	m_map.atlas = qgf::TextureManager::getSprite("data/tiles.png");
	m_map.SetMapSize(16, 16);
	defaultView = m_window.getView();
	Load();

	//m_player->health = 3;

	return true;
}

void Game::update(float deltaT)
{
	if (!playMode)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && m_window.hasFocus())
		{
			m_running = false;
		}

		//If mouse is not over UI
		if (!ImGui::GetIO().WantCaptureMouse)
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_window.hasFocus())
			{
				//Not pressing UI element
				sf::Vector2i pos = sf::Mouse::getPosition(m_window);
				int tileWidth = m_map.tileWidth;
				int tileHeight = m_map.tileHeight;
				int tileX = pos.x / tileWidth;
				int tileY = pos.y / tileHeight;
				if (m_currentTile.tileIndex == 4 && m_map.FindPlayerInMap() != NULL)
				{
					return;
				}
				if (m_currentTile.tileIndex == 1 || m_currentTile.tileIndex == 4 || m_currentTile.tileIndex == 5)
				{
					m_currentTile.blocking = true;
				}
				else {
					m_currentTile.blocking = false;
				}
				m_map.SetTile(m_currentTile, tileX, tileY);
			}
			else if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
			{
				sf::Vector2i pos = sf::Mouse::getPosition(m_window);
				int tileWidth = 32;
				int tileHeight = 32;
				int tileX = pos.x / tileWidth;
				int tileY = pos.y / tileHeight;
				Tile tile;
				tile.tileIndex = 0;
				m_map.SetTile(tile, tileX, tileY);
			}
		}

		ImGui::Begin("Kage2D");
		ImGui::Columns(3, 0, true);

		if (ImGui::Button("Save"))
		{
			Save();
		}
		ImGui::NextColumn();

		if (ImGui::Button("Load"))
		{
			Load();
		}
		ImGui::NextColumn();

		if (ImGui::Button("Clear"))
		{
			Clear();
		}
		ImGui::NextColumn();

		if (ImGui::Button("Play"))
		{
			Save();
			int playerTile = m_map.FindPlayerInMap();
			if (playerTile != NULL)
			{
				playMode = true;
				m_player->playerLocation = playerTile;
			}
			else
			{
				MessageBoxA(0, "No player found in game", "Error", 0);
			}
		}
		ImGui::NextColumn();

		//if (ImGui::Button("Resize"))
		//{
		//	//TODO: Add resize functionality maybe
		//}

		ImGui::NextColumn();

		if (ImGui::Button("Exit"))
		{
			m_running = false;
		}
		ImGui::NextColumn();

		//Draw button with sprite
		for (int i = 0; i < 5; i++)
		{
			qgf::selectSpriteTile1D(m_map.atlas, i, 32, 32);
			ImGui::PushID(i);
			if (ImGui::ImageButton(*m_map.atlas))
			{
				m_currentTile.tileIndex = i;
			}
			ImGui::PopID();
			ImGui::NextColumn();
		}
		ImGui::End();
	}
	else
	{
		if (ImGui::Button("Restart"))
		{
			Load();
		}

		if (ImGui::Button("Stop"))
		{
			playMode = false;
			Load();
		}

		/*if (playerHealth <= 0)
		{
			playMode = false;
		}*/

		int playerTile;
		playerTile = m_map.FindPlayerInMap();
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			m_player->MoveUp(&m_map);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			m_player->MoveDown(&m_map);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			m_player->MoveLeft(&m_map);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			m_player->MoveRight(&m_map);
		}

		int playerPos = m_map.FindPlayerInMap();
		int playerX = playerPos % m_map.mapWidth;
		int playerY = playerPos / m_map.mapWidth;

		sf::View view = m_window.getView();
		view.setCenter(playerX * 32, playerY * 32);
		m_window.setView(view);
	}
}

void Game::render()
{
	m_map.Render(&m_window);

	if (playMode)
	{
		sf::View view;
		float aspect = (float)m_window.getSize().x / (float)m_window.getSize().y;
		view.setSize(512, 512 / aspect);
		m_window.setView(view);
	}
	else
	{
		m_window.setView(defaultView);
	}
}

void Game::cleanup()
{

}

void Game::Load()
{
	std::fstream file;
	file.open("data/map.csv", std::ios::in);
	int x;
	int y;
	file >> x;
	file.ignore(1, ', ');
	file >> y;
	m_map.SetMapSize(x, y);
	for (int j = 0; j < y; j++)
	{
		for (int i = 0; i < x; i++)
		{
			Tile tempTile;
			file >> tempTile.tileIndex;
			file.ignore(1, ', ');
			file >> tempTile.blocking;
			file.ignore(1, ',');
			m_map.SetTile(tempTile, i, j);
		}
	}
};

void Game::Save()
{
	std::fstream file;
	file.open("data/map.csv", std::ios::out);
	file << m_map.mapWidth << ", " << m_map.mapHeight << std::endl;
	for (int y = 0; y < m_map.mapHeight; ++y)
	{
		for (int x = 0; x < m_map.mapWidth; ++x)
		{
			Tile t = m_map.GetTile(x, y);
			if (t.tileIndex == 0) {
				t.blocking = false;
			}
			file << t.tileIndex << ", " << (t.blocking ? 1 : 0);
			if (x != m_map.mapWidth - 1)
				file << ", ";
		}
		file << std::endl;
	}
};

void Game::Clear() {
	//Set the blank tile index
	Tile tile;
	tile.tileIndex = 0;

	for (int y = 0; y < m_map.mapHeight; ++y)
	{
		for (int x = 0; x < m_map.mapWidth; ++x)
		{
			m_map.SetTile(tile, x, y);
		}
	}
}