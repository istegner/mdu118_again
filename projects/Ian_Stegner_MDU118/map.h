#pragma once
#include <vector>
#include "SFML\Graphics.hpp"

struct Tile {
	unsigned int tileIndex;
	bool blocking;
};

class Map {
public:
	int mapWidth;
	int mapHeight;
	int tileWidth;
	int tileHeight;
	std::vector<Tile> tiles;
	sf::Sprite *atlas;
	Map();
	void Render(sf::RenderWindow* window);
	void SetMapSize(int width, int height);
	void SetTile(Tile tile, int x, int y);
	Tile GetTile(int x, int y);
	int FindPlayerInMap();
};