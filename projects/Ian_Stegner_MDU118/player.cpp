///Control the players behaviour

#include "game.h"
#include "player.h"
#include "map.h"

void Player::MoveUp(Map *map)
{
	int newLocation = playerLocation - map->mapWidth;
	int newX = newLocation % map->mapWidth;
	int newY = newLocation / map->mapWidth;
	Tile newTile = map->GetTile(newX, newY);
	if (newTile.blocking)
	{
		return;
	}

	Damage(newTile);

	if (newLocation <= 0)
	{
		return;
	}

	//remove the player from the map
	map->tiles[playerLocation].tileIndex = 0;
	map->tiles[playerLocation].blocking = false;
	Tile t;
	t.blocking = true;
	t.tileIndex = 4;

	//re-add the player
	map->tiles[playerLocation - map->mapWidth] = t;
	playerLocation = newLocation;
}

void Player::MoveDown(Map *map)
{
	int newLocation = playerLocation + map->mapWidth;
	int newX = newLocation % map->mapWidth;
	int newY = newLocation / map->mapWidth;
	Tile newTile = map->GetTile(newX, newY);
	if (newTile.blocking)
	{
		return;
	}

	Damage(newTile);

	//Prevent going outside of map
	if (newLocation <= 0)
	{
		return;
	}

	//remove the player from the map
	map->tiles[playerLocation].tileIndex = 0;
	map->tiles[playerLocation].blocking = false;
	Tile t;
	t.blocking = true;
	t.tileIndex = 4;

	//re-add the player to spot below them
	map->tiles[playerLocation + map->mapWidth] = t;
	playerLocation = newLocation;
}

void Player::MoveLeft(Map *map)
{
	//Check if we are at the edge of the map
	int newLocation = playerLocation - 1;
	int newX = newLocation % map->mapWidth;
	int newY = newLocation / map->mapWidth;
	Tile newTile = map->GetTile(newX, newY);
	if (newTile.blocking)
	{
		return;
	}

	Damage(newTile);

	if (newLocation <= 0)
	{
		return;
	}

	//remove the playter from the map
	map->tiles[playerLocation].tileIndex = 0;
	map->tiles[playerLocation].blocking = false;
	Tile t;
	t.blocking = true;
	t.tileIndex = 4;

	//re-add the player to the left of hem spot above them
	map->tiles[playerLocation - 1] = t;
	playerLocation = newLocation;
}

void Player::MoveRight(Map *map)
{
	int newLocation = playerLocation + 1;
	int newX = newLocation % map->mapWidth;
	int newY = newLocation / map->mapWidth;
	Tile newTile = map->GetTile(newX, newY);
	if (newTile.blocking)
	{
		return;
	}

	Damage(newTile);

	//remove the playter from the map
	map->tiles[playerLocation].tileIndex = 0;
	map->tiles[playerLocation].blocking = false;
	Tile t;
	t.blocking = true;
	t.tileIndex = 4;

	//re-add the player to the right of them spot above them
	map->tiles[playerLocation + 1] = t;
	playerLocation = newLocation;
}

void Player::Damage(Tile newTile)
{
	if (newTile.tileIndex == 3 || newTile.tileIndex == 5)
	{
		Player::health--;
	}
}