#pragma once
#include "vector"
#include "map.h"
#include "Game.h"

class Player : public Entity
{
public:
	void MoveUp(Map *map);
	void MoveDown(Map *map);
	void MoveLeft(Map *map);
	void MoveRight(Map *map);
	void Damage(Tile newTile);

	int health;

	int playerLocation;

	Map m_map;
};