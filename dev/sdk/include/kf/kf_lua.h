////////////////////////////////////////////////////////////
// KF - Kojack Framework
// Copyright (C) 2016 Kojack (rajetic@gmail.com)
//
// KF is released under the MIT License  
// https://opensource.org/licenses/MIT
////////////////////////////////////////////////////////////

#ifndef KF_LUA_HEADER
#define KF_LUA_HEADER

#include "kf/kf_types.h"
#include "kf/kf_math.h"
#include "tests/kf_test_1/luajit/lua.hpp"

namespace kf
{
	
}

#endif

